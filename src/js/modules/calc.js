;(function ($) {
    'use strict';

    const $results = $('#results'),
        $form = $('.calc-content'),
        $valueField = $('.text-field', $form);

    let calculateData = {
        currentValue: 0,
        currentPricePerMeter:  parseFloat($('.price-calc .link.active').data('price'))
    };

    $('.price-calc .tab .link').on('click', function() {
        $(this).addClass('active').siblings('.link').removeClass('active');

        calculateData.currentPricePerMeter = parseFloat($(this).data('price'));
        recalculate();
    });

    $('.price-filter .first-level').on('click', function() {
        $('.price-filter .first-level').removeClass('active');
        $(this).addClass('active');

        const target = $(this).data('target');
        $('.price-calc .tab .link[data-id="' + target + '"]').trigger('click');
    });

    $form.on('submit', function(e) {
        e.preventDefault();

        const value = parseFloat($valueField.val());

        if (value && value > 0) {
            $results.slideDown(300, function() {
                window.dispatchEvent(new Event('resize'));
            });

            calculateData.currentValue = value;
            recalculate();
        } else {

        }
    });

    UIkit.accordion('.js-accordion');

    $('.js-accordion').on('beforeshow beforehide', function() {
        setTimeout(function(){
            window.dispatchEvent(new Event('resize'));
        }, 200);
    });

    const recalculate = function() {
        $('#value').text((calculateData.currentValue).toLocaleString());
        $('#calculated-price').text((calculateData.currentValue * calculateData.currentPricePerMeter).toLocaleString());
        $('#current-price').text((calculateData.currentPricePerMeter).toLocaleString());
    }
})(jQuery);