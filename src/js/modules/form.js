;(function ($) {
    'use strict';

    const $body = $('body');

    $('.adding-area').on('click', function() {
        if (ST.checkViewport() > 3) {
            $(this).parents('.add-project').find('.focus-field').focus();
        }
    });

    $('.form').on('submit', function(e) {
        e.preventDefault();
        if ($body.hasClass('feedback-active')) {
            $('.feedback .success-message').addClass('transition');
        }

        $body.addClass('feedback-active');
        $('.feedback .success-message').addClass('shown');
    });

})(jQuery);