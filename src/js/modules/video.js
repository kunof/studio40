;(function ($) {
    'use strict';

    const $controls = $('.video-slider-controls');

    $('.certificates').owlCarousel({
        margin: 35,
        navText: ['<svg width="14px" height="24px" viewBox="0 0 14 24" xmlns="http://www.w3.org/2000/svg" data-svg="slidenav-previous"><polyline fill="none" stroke="#000" stroke-width="1.4" points="12.775,1 1.225,12 12.775,23 "></polyline></svg>','<svg width="14px" height="24px" viewBox="0 0 14 24" xmlns="http://www.w3.org/2000/svg" data-svg="slidenav-next"><polyline fill="none" stroke="#000" stroke-width="1.4" points="1.225,23 12.775,12 1.225,1 "></polyline></svg>'],
        responsiveClass: true,
        nav: true,
        dots: false,
        responsive: {
            0: {
                items: 1,
                loop: true,
            },
            768: {
                items: 2,
            },
            1080: {
                items: 3,
            }
        }
    }).on('initialized.owl.carousel', function() {
        setTimeout(function(){
            UIkit.lightbox('.image-lightbox', {animation: slide});
        }, 500);
    });

    $('.certificat-link').on('click', function(e) {e.preventDefault()});

    $('.video-slider-controls .item').on('click', function(e) {
        e.preventDefault();

        $('.video-slider-controls .item').removeClass('active');
        $(this).addClass('active');

        const offset = $(this).parent().position().top;
        TweenMax.to( $('.video-slider-controls .label'), 1.5, {delay: 0, y: offset, ease: Expo.easeInOut});
    });

    $('.nav .next', $controls).on('click', function(e) {
        e.preventDefault();
        $('.item.active', $controls).parent().next().find('.item').trigger('click');
        const id =  $('.item.active', $controls).data('id');
        UIkit.switcher($('.switcher-video')).show(id - 1);
    });

    $('.video-slider-controls .nav .prev').on('click', function(e) {
        e.preventDefault();
        $('.item.active', $controls).parent().prev().find('.item').trigger('click');
        const id =  $('.item.active', $controls).data('id');
        UIkit.switcher($('.switcher-video')).show(id - 1);
    });

    $('.video-filter .first-level').on('click', function() {
        $('.video-filter .first-level').removeClass('active');
        $(this).addClass('active');

        const target = $(this).data('target');
        UIkit.switcher($('.switcher-video')).show(target - 1);

        $('.video-slider-controls .item[data-id="' + target + '"]').trigger('click');
        window.dispatchEvent(new Event('resize'));
    });

    $('.video-preview').on('click', function() {
        const $video = $(this).parents('.video'),
            $frame = $('iframe', $video),
            src = $frame.data('src');

        $frame.attr('src', src);

        $video.addClass('active');
    });

    setTimeout(function() {
        $('.uk-active').removeAttr('style');
    }, 3000);
})(jQuery);