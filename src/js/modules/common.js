;(function ($) {
    'use strict';

    svg4everybody();

    window.ST = window.ST || {};
    const $bodyHtml = $('body,html');

    //Global functions

    if ("ontouchstart" in document.documentElement) {
        $('body').addClass('touch-device');
    }

    ST.isTouch = ("ontouchstart" in document.documentElement);

    const sm = '480',
          md = '768',
          lg = '1080',
          xl = '1480';


    ST.checkViewport = function () {
        if (window.matchMedia('(min-width: ' + xl + 'px)').matches) {
            return 5;
        } else if (window.matchMedia('(min-width: ' + lg + 'px)').matches) {
            return 4;
        } else if (window.matchMedia('(min-width: ' + md + 'px)').matches) {
            return 3;
        } else if (window.matchMedia('(min-width: ' + sm + 'px)').matches) {
            return 2;
        } else {
            return 1;
        }
    };

    ST.scrollTo = function($target) {
        const header = 60;

        $bodyHtml.stop(true).animate({
            scrollTop: ($target.offset().top - header)
        }, 800);
    };

    //Event listeners

    $('.js-to-top').on('click', function(e) {
        e.preventDefault();

        $bodyHtml.stop(true).animate({
            scrollTop: 0
        }, 800);
    });

    $('.js-scroll-to').on('click', function(e) {
        e.preventDefault();

        const id = $(this).data('href') || $(this).attr('href');

        if (id) {
            if ($(id).length) {
                ST.scrollTo($(id));
            }
        }
    });

    $('.js-scroll-to-steps').on('click', function(e) {
        e.preventDefault();

        const id = $(this).data('href') || $(this).attr('href');

        if (id) {
            if ($(id).length) {
                const header = 60;

                if (ST.checkViewport() < 4) {
                    $bodyHtml.stop(true).animate({
                        scrollTop: ($(id).offset().top - header)
                    }, 800);
                } else {
                    $bodyHtml.stop(true).animate({
                        scrollTop: ($('.h1', $(id).parent()).offset().top - $(window).height() / 2)
                    }, 800);
                }
            }
        }
    });

    //Line animation

    const nav = document.querySelectorAll('nav')[0];
    const navTrackActive = document.querySelectorAll('.nav-track__active')[0];
    let navItems = document.querySelectorAll('.main-nav .link');
    let timeoutMouseLeave = null;

    const hoverItem = function() {
        clearTimeout(timeoutMouseLeave);
        for (let i = 0; i < navItems.length; i++){
            navItems[i].classList.remove('active');
        }
        this.classList.add('active');

        navTrackActive.style.width = this.offsetWidth - 20 + 'px';
        navTrackActive.style.opacity = 1;
        navTrackActive.style.left = this.offsetLeft + 10 + 'px';
    };

    const blurItems = function() {
        navTrackActive.style.opacity = 0;
        for (let i = 0; i < navItems.length; i++){
            navItems[i].classList.remove('active');
        }
    };

    for (let i = 0; i < navItems.length; i++) {
        navItems[i].onmouseover = hoverItem.bind(navItems[i]);
        navItems[i].focus = hoverItem.bind(navItems[i]);
    }

    nav.onmouseleave = function() {
        clearTimeout(timeoutMouseLeave);
        timeoutMouseLeave = setTimeout(function () {
            blurItems()
        }, 300)
    };

    $bodyHtml.on('click', 'a', function(e) {
        let flag = false;

        const $this = $(this);

        if ($this.hasClass('link-ab') || $this.attr('target') || $this.attr('href').indexOf('#') !== -1  || $this.attr('href').indexOf('tel') !== -1) {
            flag = true;
        }

        if (!flag) {
            let href = $(this).attr('href');

            if (href) {
                e.preventDefault();
                TweenMax.to($('.loader'), .5, {opacity: 1});
                setTimeout(function() {window.location = href}, 400);
            }
        }
    });

    //Team
    ST.sliderResize = function($element) {
        const $items = $('.item', $element),
            $slideshow = $('.uk-slideshow-items', $element);

        let height = 0;

           $items.each(function() {
               if ($(this).height() > height) {
                   height = $(this).height();
               }
           });

        $slideshow.css('height', height);
    };

    const $teamCarousel = $('.team-carousel, .slideshow-comparing');

    $(window).on('resize scroll', function() {
        ST.sliderResize($teamCarousel);
        $('.compare').each(function() {
            $('.image:first-child img', $(this)).width($(this).width());
        });
    });

    if ($teamCarousel) {
        if ($teamCarousel.length) {
            setInterval(function(){
                ST.sliderResize($teamCarousel);
            }, 10);
        }
    }

    var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

    const changeHeight = function () {
        $('.project-visual').css('height', $(window).height());
    };

    $(window).on('resize', function() {
        if (iOS) {
            changeHeight();
        }
    });

    if (iOS) {
        changeHeight();
    }

    window.onpageshow = function(event) {
        if (event.persisted) {
            TweenMax.to($('.loader'), .5, {opacity: 0});
            $('body').removeClass('overlay-active');
        }
    };

})(jQuery);