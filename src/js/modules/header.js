;(function ($) {
    'use strict';

    const $window = $(window),
        $html = $('html'),
        $body = $('body');

    //Page scroll
    const checkPageScroll = function() {
        ST.scrollTop = $window.scrollTop();
        $html.toggleClass('page-scrolled', ST.scrollTop > 0);
    };


    $('.js-navigation-open').on('click', function(e) {
        e.preventDefault();

        $body.toggleClass('overlay-active');
    });

    $('.js-navigation-close').on('click', function(e) {
        e.preventDefault();

        $body.removeClass('overlay-active');
    });

    const changeHeaderColor = function () {
        $('section').each(function() {
            const $this = $(this),
                $header = $('.header'),
                offset = $this.offset().top,
                height = $this.outerHeight();

            if (ST.scrollTop > offset && ST.scrollTop < offset + height) {
                if ($this.hasClass('dark')) {
                    $header.addClass('dark');
                } else {
                    $header.removeClass('dark');
                }
            }
        });
    };

    $window.on('scroll', function() {
        checkPageScroll();
        //changeHeaderColor();
    });

    checkPageScroll();
})(jQuery);