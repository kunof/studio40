;(function ($) {
    'use strict';

    const $body = $('body');

    $('.js-feedback-open').on('click', function(e) {
        e.preventDefault();

        $body.addClass('feedback-active');
    });

    $('.js-feedback-close').on('click', function(e) {
        e.preventDefault();

        $body.removeClass('feedback-active');
        setTimeout(function() {
            $('.feedback .success-message').removeClass('shown transition');
        }, 300);
    });

})(jQuery);