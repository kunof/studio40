;(function ($) {
    'use strict';

    $('.clients .items').owlCarousel({
        loop: true,
        responsiveClass: true,
        autoplayTimeout: 3000,
        autoplay: true,
        slideTransition: 'linear',
        touchDrag: false,
        mouseDrag: false,
        smartSpeed : 3000,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 2,
            },
            768: {
                items: 4,
            },
            1080: {
                items: 6,
            }
        }
    });
})(jQuery);