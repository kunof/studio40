;(function ($) {
    'use strict';

    $('.points-carousel').owlCarousel({
        margin: 35,
        navText: ['<svg width="14px" height="24px" viewBox="0 0 14 24" xmlns="http://www.w3.org/2000/svg" data-svg="slidenav-previous"><polyline fill="none" stroke="#000" stroke-width="1.4" points="12.775,1 1.225,12 12.775,23 "></polyline></svg>','<svg width="14px" height="24px" viewBox="0 0 14 24" xmlns="http://www.w3.org/2000/svg" data-svg="slidenav-next"><polyline fill="none" stroke="#000" stroke-width="1.4" points="1.225,23 12.775,12 1.225,1 "></polyline></svg>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                mouseDrag: true,
                dots: true,
                loop: true,
                nav: true
            },
            768: {
                items: 2,
                nav: true,
                mouseDrag: true,
            },
            1080: {
                items: 4,
                mouseDrag: false,
            }
        }
    });
})(jQuery);