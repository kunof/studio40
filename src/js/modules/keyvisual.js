;(function ($) {
    'use strict';

    $('.keyvisual-slider').owlCarousel({
        items: 1,
        autoplay: true,
        nav: false,
        dots: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        loop: true,
        touchDrag: false,
        autoplayTimeout: 2500,
        mouseDrag: false
    });

    const $patterns =   $('.bird-image .path-pattern');
    const $bird =   $('.bird-image');

    $patterns.on('click', function() {
        const href = $(this).data('href');

        TweenMax.to($('.loader'), .5, {opacity: 1});
        setTimeout(function() {window.location = href}, 400);
    });

    $patterns.on('mouseenter', function() {
        $bird.addClass('moving');

        const data = $(this).data('siblings'),
            id = $(this).attr('id');

        $('#' + id + '-text').addClass('active');

        if (typeof data === "object") {
            $patterns.css('fill-opacity', 0);
            $(this).css('fill-opacity', 1);
            for (let [key, value] of Object.entries(data)) {
                $('#' + key).css('fill-opacity', value);
            }
        }

    });

    $patterns.on('mouseleave', function() {
        $(this).removeClass('opacity-1');
        $patterns.css('fill-opacity', 0);
        $bird.removeClass('moving');
        $('.image .caption').removeClass('active');
    });

})(jQuery);