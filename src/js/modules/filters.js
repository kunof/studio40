;(function ($) {
    'use strict';

    $('body').on('click', '.hashtags .link, .modal-filter .link', function(e) {
        e.preventDefault();

        const $this = $(this);

        $this.addClass('active').siblings('.link').removeClass('active');
    });

})(jQuery);
