;(function ($) {
    'use strict';

    $('.geography .marker-geo').on('click', function() {
        $(this).parents('.point').addClass('active').siblings('.point').removeClass('active');

        const left =  $(this).parents('.point').position().left - 110,
            ww = $(window).width() / 2,
            elW = $(this).parents('.point').find('.point-tooltip').width() / 2;

        $('.geo-container').stop(true).animate({
            scrollLeft: left - ww + elW
        }, 400);
    });

    $('.geography .point .close').on('click', function() {
        $(this).parents('.point').removeClass('active');
    });



})(jQuery);
