;(function ($) {
    'use strict';

    TweenMax.to($('.loader'), .5, {delay: .3, opacity: 0});

    //Keyvisual - without parallax || Problem with window resize
    const $keyvisual = $('.keyvisual');
    if ($keyvisual.length) {
        if (ST.checkViewport() > 3) {
            TweenMax.from($('.image', $keyvisual), 2, {delay: 0, opacity: 0, ease: Expo.easeInOut});
            TweenMax.from($('.title', $keyvisual), 2, {delay: 1, x: 80, opacity: 0, ease: Expo.easeInOut});
            TweenMax.from($('.link-1', $keyvisual), 2, {delay: 1.3, x: 80, opacity: 0, ease: Expo.easeInOut});
            TweenMax.from($('.link-1 .line', $keyvisual), 2.2, {delay: 1.5, x: 50});
            TweenMax.from($('.link-2', $keyvisual), 2, {delay: 1.5, x: 80, opacity: 0, ease: Expo.easeInOut});
            TweenMax.from($('.link-2 .line', $keyvisual), 2.2, {delay: 1.7, x: 50});
            TweenMax.from($('.scroll-to', $keyvisual), 1.2, {delay: 3, opacity: 0});
            TweenMax.from($('.social-links', $keyvisual), 1.2, {delay: 3, opacity: 0, y: 50});

            //HEADER
            TweenMax.from($('.homepage .logo'), 1.2, {delay: 3, opacity: 0});
            TweenMax.from($('.homepage .main-nav .link'), 1.2, {delay: 3.2, opacity: 0});
            TweenMax.from($('.homepage .hamburger-wrapper'), 1.2, {delay: 3.2, opacity: 0});
            TweenMax.from($('.homepage .right-side'), 1.2, {delay: 3.6, opacity: 0});
            TweenMax.from($('.homepage .feedback-button .line'), .6, {delay: 3.8, width: 0, ease: Expo.easeInOut});
        }
    }

    //END Keyvisual - without parallax


    //Steps parallax
    const $scrollContainer = $('#scroll-container');
    var triggerOffset = document.documentElement.clientHeight / 4.5 * 3;
    var requestId = null;
    var tl = gsap.timeline({ paused: true });

    if ($('.steps').length) {
        setTimeout(function() {
            $('.steps .step').each(function () {
                const $this = $(this);
                const sceneStart = $this.offset().top;
                const duration = 3067;

                tl.to($('.number', $this), {duration: duration / 3 * 2, color: '#000', borderColor: '#000'}, sceneStart)
                    .to($('.number', $this), {duration: duration, y: ($this.height() / 5 * 3),}, sceneStart)
                    .to($('.text', $this), {duration: duration, y: -250,}, sceneStart)
                    .to($('.image-cover img', $this), {
                        duration: duration,
                        y: -1 * $('.image-cover', $this).height() / 100 * 15,
                    }, sceneStart);
            });
        }, 300);
    }
    //END Steps parallax


    //Image slider
    const $imageSliders = $('.anim-wrapper');
    if ($imageSliders.length) {
        setTimeout(function() {
            $imageSliders.each(function() {
                const $this = $(this);
                const sceneStart = $this.offset().top - $this.height() / 4;
                tl.set($this, { className: "+=showed" }, sceneStart)
            });
        }, 300);
    }
    //END Image slider

    // Get questions parallax
    const $question = $('.get-question');
    if ($question.length) {
        setTimeout(function() {
            const $this = $('.image', $question);
            const sceneStart = $this.offset().top - document.documentElement.clientHeight / 4.5;
            const duration = $('.image-wrap', $question).height();
            tl.to($this, { duration: duration, scale: .9 }, sceneStart)
                .to($('.name-1, .name-2', $question), {duration: duration / 3, opacity: 1 }, sceneStart + duration / 2)
                .to($('.name-1,  .name-2', $question), {duration: duration, y: 50 }, sceneStart + duration / 3 * 2)
                .set($('.form-wrapper', $question), { className: "+=show" }, $('.form-wrapper', $question).offset().top - document.documentElement.clientHeight / 6)
                .from($('.container', $question), { duration: duration, y: -100 }, sceneStart);
        }, 300);
    }
    // END get questions parallax

    // Geography parallax
    const $geography = $('.geography');
    if ($geography.length) {
        setTimeout(function() {
            const $this = $('.geo-container', $geography);
            const sceneStart = $this.offset().top - document.documentElement.clientHeight / 2;
            const duration = document.documentElement.clientHeight;

            tl.to($('.image', $this), { duration: duration, y: -100 }, sceneStart)
            .to($('.point', $this), { duration: duration, y: 0 }, sceneStart)
            .to($this.parents('.container'), { duration: duration + 100, y: 150 }, sceneStart + duration);
        }, 300);
    }
    // END Geography parallax

    // Home comparing parallax
    const $homeComparing = $('.homepage-comparing');
    if ($homeComparing.length) {
        setTimeout(function() {
            const $this = $homeComparing;
            const sceneStart = $this.offset().top - triggerOffset;
            const duration = document.documentElement.clientHeight;

            tl.from($('.container', $this), { duration: duration * 2, y: -150 }, sceneStart);
        }, 300);
    }
    // END Home comparing parallax

    // Video list parallax
    const $videosList = $('.video-slider-controls');
    if ($videosList.length) {
        setTimeout(function() {
            const $this = $videosList;
            const sceneStart = $this.offset().top - triggerOffset;
            const duration = document.documentElement.clientHeight;

            tl.from($this, { duration: duration, y: -150 }, sceneStart);
        }, 300);
    }
    // END Video list parallax

    // Clients parallax
    const $clients = $('.clients');
    if ($videosList.length) {
        setTimeout(function() {
            const $this = $clients;
            const sceneStart = $this.offset().top +  triggerOffset;
            const duration = document.documentElement.clientHeight;

            tl.to($this, { duration: duration, y: 200 }, sceneStart);
        }, 300);
    }
    // END Clients parallax

    //Parallax filler
    const $fillers = $('.parallax-filler');
    if ($fillers.length) {
        $fillers.each(function() {
            const $this = $(this);
            const sceneStart = $this.offset().top  +  document.documentElement.clientHeight / 2;
            const duration = document.documentElement.clientHeight * 2;

            tl.to($this, { duration: duration, scale: 5 }, sceneStart)
        });
    }
    //ENDParallax filler

    //Short text parallax
    const $shortText = $('.short-text');
    if ($shortText.length) {
        $shortText.each(function() {
            const $this = $('.container', $(this));
            const sceneStart = $this.offset().top -  document.documentElement.clientHeight / 2;
            const duration = document.documentElement.clientHeight;

            tl.to($this, { duration: duration, y: -100 }, sceneStart)
        });
    }

    //END Short text parallax

    //Points parallax
    const $points = $('.points');
    if ($points.length) {
        $points.each(function() {
            const $this = $(this);
            const sceneStart = $('.line', $this).eq(0).offset().top -  document.documentElement.clientHeight / 2;
            const duration = document.documentElement.clientHeight / 2;

            tl.to($('.line', $this), { duration: duration, x: 0 }, sceneStart)
        });
    }
    //END Points parallax

    update();

    setInterval(function() {
        if (!requestId) {
            requestId = requestAnimationFrame(update);
        }
    }, 3);

    function update() {
        if (!ST.isTouch) {
            tl.time(($scrollContainer.position().top * -1) + triggerOffset);
        } else {
            tl.time(window.pageYOffset + triggerOffset);
        }
        requestId = null;
    }


})(jQuery);